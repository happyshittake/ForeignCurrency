package repo_test

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/stretchr/testify/suite"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/models"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/repo"
	"log"
	"os"
	"testing"
	"time"
)

type RepoTestSuite struct {
	suite.Suite

	DB   *gorm.DB
	Repo repo.Contract
}

func (ts *RepoTestSuite) SetupSuite() {
	userDB := os.Getenv("TEST_DB_USERNAME")
	passwordDB := os.Getenv("TEST_DB_PASSWORD")
	nameDB := os.Getenv("TEST_DB_NAME")
	hostDB := os.Getenv("TEST_DB_HOST")
	portDB := os.Getenv("TEST_DB_PORT")
	sslmodeDB := os.Getenv("TEST_DB_SSL")

	dbDSN := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		hostDB,
		portDB,
		userDB,
		nameDB,
		passwordDB,
		sslmodeDB,
	)

	var err error = nil
	ts.DB, err = gorm.Open("postgres", dbDSN)

	if err != nil {
		log.Fatal(err)
	}
	ts.Repo = &repo.GormRepo{DB: ts.DB.Debug()}
}

func (ts *RepoTestSuite) BeforeTest(suiteName, testName string) {
	ts.DB.DropTableIfExists(&models.Rate{}, &models.Exchange{})
	ts.DB.AutoMigrate(&models.Exchange{}, &models.Rate{})
}

func (ts *RepoTestSuite) AfterTest(suiteName, testName string) {
	ts.DB.DropTableIfExists(&models.Rate{}, &models.Exchange{})
}

func (ts *RepoTestSuite) TearDownSuite() {
	ts.DB.Close()
}

func (ts *RepoTestSuite) TestFindExchangeByToAndFrom() {
	tb := []struct {
		SeedData []models.Exchange
		TestData *models.Exchange
		Err      error
	}{
		{
			[]models.Exchange{
				{
					From: "IDR",
					To:   "JPY",
				},
			},
			&models.Exchange{
				From: "IDR",
				To:   "JPY",
			},
			nil,
		},
	}

	for _, td := range tb {
		for _, d := range td.SeedData {
			ts.DB.Create(&d)
		}

		_, err := ts.Repo.FindExchangeByToAndFrom(td.TestData.From, td.TestData.To)

		ts.Equal(td.Err, err)
	}
}

func (ts *RepoTestSuite) TestCreateExchange() {
	tb := []struct {
		Data  []models.Exchange
		Rates []models.Rate
		Err   error
	}{
		{
			[]models.Exchange{
				{
					From: "JPY",
					To:   "USD",
				},
			},
			nil,
			nil,
		},
		{
			[]models.Exchange{
				{
					From: "",
					To:   "",
				},
			},
			nil,
			repo.ErrStringFieldIsEmpty,
		},
	}

	for _, td := range tb {
		if len(td.Data) < 2 {
			ts.Equal(td.Err, ts.Repo.CreateExchange(&td.Data[0], td.Rates))
		} else {
			if err := ts.DB.Debug().Create(&td.Data[0]).Error; err != nil {
				log.Fatal(err)
			}

			ts.Equal(td.Err, ts.Repo.CreateExchange(&td.Data[1], td.Rates))

			var dts []models.Exchange

			ts.DB.Debug().Find(&dts, map[string]interface{}{"from": td.Data[0].From, "to": td.Data[0].To})

			ts.Equal(1, len(dts), "got value: %v", dts)
		}
	}
}

func (ts *RepoTestSuite) TestAddRateToExchange() {
	tb := []struct {
		SeedData *models.Exchange
		Rate     *models.Rate
		Err      error
	}{
		{
			&models.Exchange{
				To:   "IDR",
				From: "MYR",
			},
			&models.Rate{
				Rate: 0.678,
				Date: time.Now().Local(),
			},
			nil,
		},
		{
			nil,
			&models.Rate{
				Rate: 0.678,
				Date: time.Now().Local(),
			},
			gorm.ErrRecordNotFound,
		},
	}

	for _, td := range tb {
		if td.SeedData == nil {
			ts.Equal(td.Err, ts.Repo.AddRateToExchange(uint(97), td.Rate))
			continue
		}

		ts.DB.Debug().Create(td.SeedData)

		ts.Equal(td.Err, ts.Repo.AddRateToExchange(td.SeedData.ID, td.Rate))
	}
}

func (ts *RepoTestSuite) TestDeleteExchange() {
	tb := []struct {
		Target *models.Exchange
		Err    error
	}{
		{
			&models.Exchange{
				From: "IDR",
				To:   "JPY",
			},
			nil,
		},
	}

	for _, td := range tb {
		ts.DB.Create(td.Target)

		ts.Equal(td.Err, ts.Repo.DeleteExchange(td.Target.ID))

		count := 3
		ts.DB.Debug().Model(td.Target).Count(&count)

		ts.Equal(0, count)
	}
}

func (ts *RepoTestSuite) TestGetExchangeRatesByDate() {
	d := time.Now().Local()

	tb := []struct {
		Seeds []models.Exchange
		Err   error
	}{
		{
			[]models.Exchange{
				{
					To:   "USD",
					From: "IDR",
					Rates: []models.Rate{
						{
							Rate: 0.876,
							Date: d,
						},
						{
							Rate: 0.876,
							Date: d.Truncate(24 * time.Hour),
						},
						{
							Rate: 0.876,
							Date: d.Truncate(2 * 24 * time.Hour),
						},
					},
				},
			},
			nil,
		},
	}

	for _, td := range tb {
		for _, data := range td.Seeds {
			ts.DB.Debug().Create(&data)
		}

		exchanges, err := ts.Repo.GetExchangeRatesByDate(&d)
		ts.Equal(td.Err, err)
		ts.Equal(3, len(exchanges[0].Rates))
	}
}

func (ts *RepoTestSuite) TestGetWeeklyRate() {
	d := time.Now().Local()

	tb := []struct {
		Seeds []models.Exchange
		Err   error
	}{
		{
			[]models.Exchange{
				{
					To:   "USD",
					From: "IDR",
					Rates: []models.Rate{
						{
							Rate: 0.876,
							Date: d,
						},
						{
							Rate: 0.876,
							Date: d.Truncate(24 * time.Hour),
						},
						{
							Rate: 0.876,
							Date: d.Truncate(2 * 24 * time.Hour),
						},
					},
				},
			},
			nil,
		},
	}

	for _, td := range tb {
		for _, data := range td.Seeds {
			ts.DB.Debug().Create(&data)
		}

		rates, err := ts.Repo.GetWeeklyRate(td.Seeds[0].From, td.Seeds[0].To)
		ts.Equal(td.Err, err)
		ts.Equal(3, len(rates))
	}
}

func (ts *RepoTestSuite) TestGetExchanges() {
	seeds := []models.Exchange{
		{From: "IDR", To: "USD"},
		{From: "IDR2", To: "USD2"},
		{From: "IDR3", To: "USD3"},
		{From: "IDR4", To: "USD4"},
		{From: "IDR5", To: "USD5"},
	}

	for _, v := range seeds {
		ts.DB.Debug().Create(&v)
	}

	exchanges := ts.Repo.GetExchanges()

	ts.Equal(5, len(exchanges))
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(RepoTestSuite))
}
