package repo

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/models"
	"strings"
	"time"
)

var ErrStringFieldIsEmpty = errors.New("string field must not be empty")
var ErrExchangeAlreadyExists = errors.New("exchange already exists")

type Contract interface {
	FindExchangeByToAndFrom(from, to string) (*models.Exchange, error)
	CreateExchange(exchange *models.Exchange, rates []models.Rate) error
	AddRateToExchange(exchangeID uint, rate *models.Rate) error
	DeleteExchange(exchangeID uint) error
	GetExchangeRatesByDate(t *time.Time) ([]models.Exchange, error)
	GetWeeklyRate(from, to string) ([]models.Rate, error)
	GetExchanges() []models.Exchange
}

type GormRepo struct {
	DB *gorm.DB
}

func (g *GormRepo) GetExchanges() []models.Exchange {
	items := []models.Exchange{}

	g.DB.Find(&items)

	return items
}

func (g *GormRepo) GetWeeklyRate(from, to string) ([]models.Rate, error) {
	exchange, err := g.FindExchangeByToAndFrom(from, to)
	if err != nil {
		return nil, err
	}

	if exchange == nil {
		return nil, errors.New("record not found")
	}

	t := time.Now().Local()

	var rates []models.Rate

	err = g.DB.
		Order("date desc").
		Limit(7).
		Find(&rates, "exchange_id = ? AND date <= ? AND date > ?", exchange.ID, t, t.Truncate(7*24*time.Hour)).
		Error
	if err != nil {
		return nil, err
	}

	return rates, nil
}

func (g *GormRepo) GetExchangeRatesByDate(t *time.Time) ([]models.Exchange, error) {
	var exchanges []models.Exchange

	err := g.DB.Preload("Rates", func(db *gorm.DB) *gorm.DB {
		return db.Where("date <= ? AND date > ?", t, t.Truncate(7*24*time.Hour)).Order("date desc")
	}).Find(&exchanges).Error

	if err != nil {
		return nil, err
	}

	return exchanges, nil
}

func (g *GormRepo) DeleteExchange(exchangeID uint) error {
	if err := g.DB.Delete(&models.Exchange{}, "id=?", exchangeID).Error; err != nil {
		return err
	}

	return nil
}

func (g *GormRepo) FindExchangeByToAndFrom(from, to string) (*models.Exchange, error) {
	if !stringMustNotEmpty(from, to) {
		return nil, ErrStringFieldIsEmpty
	}

	exchange := &models.Exchange{}

	err := g.DB.Where(&models.Exchange{From: from, To: to}).First(exchange).Error

	if err != nil {
		return nil, err
	}

	return exchange, nil
}

func (g *GormRepo) CreateExchange(exchange *models.Exchange, rates []models.Rate) error {
	if !stringMustNotEmpty(exchange.To, exchange.From) {
		return ErrStringFieldIsEmpty
	}

	exchange.Rates = rates
	exchange.To = strings.ToUpper(exchange.To)
	exchange.From = strings.ToUpper(exchange.From)

	if err := g.DB.Create(exchange).Error; err != nil {
		return err
	}

	return nil
}

func (g *GormRepo) AddRateToExchange(exchangeID uint, rate *models.Rate) error {
	rate.ExchangeID = exchangeID

	exchange := &models.Exchange{}

	if err := g.DB.First(exchange, "id=?", exchangeID).Error; err != nil {
		return err
	}

	if err := g.DB.Create(rate).Error; err != nil {
		return err
	}

	return nil
}

func stringMustNotEmpty(fields ...string) bool {
	for _, v := range fields {
		if len(v) < 1 {
			return false
		}
	}

	return true
}
