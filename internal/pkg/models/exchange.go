package models

import (
	"github.com/jinzhu/gorm"
)

type Exchange struct {
	gorm.Model
	From  string `json:"from" gorm:"NOT NULL"`
	To    string `json:"to" gorm:"NOT NULL"`
	Rates []Rate `json:"rates" gorm:"NOT NULL"`
}
