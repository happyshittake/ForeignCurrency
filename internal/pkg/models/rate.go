package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Rate struct {
	gorm.Model
	Rate       float64   `gorm:"NOT NULL" json:"rate"`
	Date       time.Time `gorm:"NOT NULL" json:"date"`
	ExchangeID uint      `gorm:"NOT NULL" json:"exchange_id"`
}
