package bootstrap

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/happyshittake/ForeignCurrency/internal/app/foreign-currency/handler"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/models"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/repo"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/validate"
	"gopkg.in/go-playground/validator.v9"
)

type Params struct {
	DB *gorm.DB
}

func Bootstrap(p *Params) *echo.Echo {
	p.DB.AutoMigrate(models.Exchange{}, models.Rate{})

	e := echo.New()
	e.Validator = &validate.Validator{Validator: validator.New()}
	e.Use(middleware.Logger())
	h := handler.Handler{
		Repo: &repo.GormRepo{DB: p.DB.Debug()},
	}
	e.GET("/exchanges", h.GetExchanges)
	e.POST("/exchanges", h.StoreExchange)
	e.DELETE("/exchanges/:id", h.DeleteExchange)

	e.GET("/exchange-rate/:date", h.GetTrackedExchangeRates)
	e.POST("/week-data", h.GetWeekData)
	e.POST("/daily-exchange", h.StoreDailyExchangeData)

	return e
}
