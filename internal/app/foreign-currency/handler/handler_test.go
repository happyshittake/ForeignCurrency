package handler_test

import (
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/happyshittake/ForeignCurrency/internal/app/foreign-currency/dtos"
	"gitlab.com/happyshittake/ForeignCurrency/internal/app/foreign-currency/handler"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/models"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/validate"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

type MockRepo struct {
	mock.Mock
}

func (m *MockRepo) GetExchanges() []models.Exchange {
	args := m.Called()
	return args.Get(0).([]models.Exchange)
}

func (m *MockRepo) FindExchangeByToAndFrom(from, to string) (*models.Exchange, error) {
	args := m.Called(from, to)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*models.Exchange), args.Error(1)
}

func (m *MockRepo) CreateExchange(exchange *models.Exchange, rates []models.Rate) error {
	var args mock.Arguments
	if rates == nil {
		args = m.Called(exchange, nil)
	} else {
		args = m.Called(exchange, rates)
	}
	return args.Error(0)
}

func (m *MockRepo) AddRateToExchange(exchangeID uint, rate *models.Rate) error {
	args := m.Called(exchangeID, rate)
	return args.Error(0)
}

func (m *MockRepo) DeleteExchange(exchangeID uint) error {
	args := m.Called(exchangeID)
	return args.Error(0)
}

func (m *MockRepo) GetExchangeRatesByDate(t *time.Time) ([]models.Exchange, error) {
	args := m.Called(t)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]models.Exchange), args.Error(1)
}

func (m *MockRepo) GetWeeklyRate(from, to string) ([]models.Rate, error) {
	args := m.Called(from, to)
	return args.Get(0).([]models.Rate), args.Error(1)
}

func TestDeleteExchange(t *testing.T) {
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.DELETE, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/exchange/:id")
	c.SetParamNames("id")
	c.SetParamValues("3")
	h := &handler.Handler{mockRepo}

	mockRepo.On("DeleteExchange", uint(3)).Return(nil)

	if assert.NoError(t, h.DeleteExchange(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		mockRepo.AssertExpectations(t)
	}
}

func TestGetExchanges(t *testing.T) {
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.GET, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := &handler.Handler{mockRepo}

	data := []models.Exchange{
		{From: "IDR", To: "USD"},
		{From: "IDR2", To: "USD2"},
		{From: "IDR3", To: "USD3"},
		{From: "IDR4", To: "USD4"},
	}

	mockRepo.On("GetExchanges").Return(data)

	if assert.NoError(t, h.GetExchanges(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		mockRepo.AssertExpectations(t)
	}
}

func TestStoreExchange(t *testing.T) {
	requestData := `
	{
		"from": "IDR",
		"to": "USD"
	}
	`
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.POST, "/", strings.NewReader(requestData))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := &handler.Handler{mockRepo}

	mockRepo.On("CreateExchange", &models.Exchange{To: "USD", From: "IDR"}, nil).Return(nil)

	if assert.NoError(t, h.StoreExchange(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		mockRepo.AssertExpectations(t)
	}
}

func TestGetWeekData(t *testing.T) {
	reqJSON := `
	{
		"from": "USD",
		"to": "IDR"
	}
	`
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.POST, "/", strings.NewReader(reqJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := &handler.Handler{mockRepo}

	data := []models.Rate{
		{
			Rate: 10000.0,
			Date: time.Now(),
		},
		{
			Rate: 12000.0,
			Date: time.Now(),
		},
		{
			Rate: 13000.0,
			Date: time.Now(),
		},
	}

	mockRepo.On("GetWeeklyRate", "USD", "IDR").Return(data, nil)

	if assert.NoError(t, h.GetWeekData(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		mockRepo.AssertExpectations(t)
	}
}

func TestGetTrackedExchangeRates(t *testing.T) {
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.GET, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/exchange-rate/:date")
	c.SetParamNames("date")
	c.SetParamValues("2010-02-02")
	h := &handler.Handler{mockRepo}

	date, _ := time.Parse(dtos.DateLayout, "2010-02-02")
	responseObj := []models.Exchange{
		{
			From: "IDR",
			To:   "USD",
			Rates: []models.Rate{
				{Rate: 0.922, Date: time.Now()},
				{Rate: 0.875, Date: time.Now()},
				{Rate: 0.231, Date: time.Now()},
			},
		},
		{
			From:  "AUS",
			To:    "USD",
			Rates: []models.Rate{},
		},
	}

	responseJson := `
		[{
			"to": "USD",
			"rate": 0.922,
			"avg": 0.676,
			"from": "IDR"
		},{
			"to": "USD",
			"rate": "insufficient data",
			"avg": "insufficient data",
			"from": "AUS"
		}]
	`

	mockRepo.On("GetExchangeRatesByDate", &date).Return(responseObj, nil)

	if assert.NoError(t, h.GetTrackedExchangeRates(c)) {
		assert.JSONEq(t, responseJson, rec.Body.String())
	}

	mockRepo.AssertExpectations(t)
}

func TestSuccessCreateDailyExchangeData(t *testing.T) {
	requestData := `
	{
		"date": "2018-09-02",
		"from": "IDR",
		"to": "USD",
		"rate": 0.9292
	}
	`
	mockRepo := &MockRepo{}
	valid := &validate.Validator{validator.New()}
	e := echo.New()
	e.Validator = valid
	req := httptest.NewRequest(echo.POST, "/", strings.NewReader(requestData))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := &handler.Handler{mockRepo}

	mockRepo.On("FindExchangeByToAndFrom", "IDR", "USD").Return(nil, nil)
	mockRepo.On("CreateExchange", &models.Exchange{To: "USD", From: "IDR"}, nil).Return(nil)
	parsedTime, _ := time.Parse(dtos.DateLayout, "2018-09-02")
	rateObj := &models.Rate{Date: parsedTime, Rate: 0.9292}
	mockRepo.On("AddRateToExchange", uint(0), rateObj).Return(nil)

	if assert.NoError(t, h.StoreDailyExchangeData(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}

	mockRepo.AssertExpectations(t)
}
