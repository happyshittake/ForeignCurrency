package handler

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"gitlab.com/happyshittake/ForeignCurrency/internal/app/foreign-currency/dtos"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/models"
	"gitlab.com/happyshittake/ForeignCurrency/internal/pkg/repo"
	"math"
	"net/http"
	"strconv"
	"time"
)

type Handler struct {
	Repo repo.Contract
}

func (h *Handler) bindAndValidate(c echo.Context, obj interface{}) error {
	if err := c.Bind(obj); err != nil {
		return err
	}
	if err := c.Validate(obj); err != nil {
		return err
	}

	return nil
}

func (h *Handler) GetExchanges(c echo.Context) error {
	exchanges := h.Repo.GetExchanges()

	return c.JSON(http.StatusOK, exchanges)
}

func (h *Handler) DeleteExchange(c echo.Context) error {
	ID := c.Param("id")
	id, err := strconv.Atoi(ID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "invalid id")
	}

	err = h.Repo.DeleteExchange(uint(id))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "failed to delete")
	}

	return c.NoContent(http.StatusOK)
}

func (h *Handler) StoreExchange(c echo.Context) error {
	reqObj := &dtos.WeekLastDataRequest{}

	if err := h.bindAndValidate(c, reqObj); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	err := h.Repo.CreateExchange(&models.Exchange{From: reqObj.From, To: reqObj.To}, nil)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "error creating record")
	}

	return c.NoContent(http.StatusCreated)
}

func (h *Handler) GetWeekData(c echo.Context) error {
	reqObj := &dtos.WeekLastDataRequest{}

	if err := h.bindAndValidate(c, reqObj); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	rates, err := h.Repo.GetWeeklyRate(reqObj.From, reqObj.To)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	respObj := &dtos.WeekLastDataResponse{}

	if len(rates) < 1 {
		respObj.Avg = "insufficient data"
		respObj.Variance = "insufficient data"
		respObj.Rates = []dtos.WeekItem{}

		return c.JSON(http.StatusOK, respObj)
	}

	respObj.Avg = avgRates(rates)
	respObj.Variance = findVariance(rates)
	items := []dtos.WeekItem{}
	for _, v := range rates {
		date := dtos.Datetime{}
		date.Time = v.Date
		items = append(items, dtos.WeekItem{
			Date: date,
			Rate: v.Rate,
		})
	}
	respObj.Rates = items

	return c.JSON(http.StatusOK, respObj)
}

func (h *Handler) GetTrackedExchangeRates(c echo.Context) error {
	date := c.Param("date")
	dateTime, err := time.Parse(dtos.DateLayout, date)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	exchanges, err := h.Repo.GetExchangeRatesByDate(&dateTime)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	response := []dtos.TrackedExchangeRateObject{}
	for _, exchange := range exchanges {
		item := dtos.TrackedExchangeRateObject{}
		item.From = exchange.From
		item.To = exchange.To
		if len(exchange.Rates) < 1 {
			item.Avg = "insufficient data"
			item.Rate = "insufficient data"
		} else {
			item.Avg = avgRates(exchange.Rates)
			item.Rate = exchange.Rates[0].Rate
		}

		response = append(response, item)
	}

	return c.JSON(http.StatusOK, response)
}

func findVariance(rates []models.Rate) float64 {
	max := 0.0
	min := math.MaxFloat64
	for _, v := range rates {
		if v.Rate > max {
			max = v.Rate
		}
		if v.Rate < min {
			min = v.Rate
		}
	}

	return max - min
}

func avgRates(rates []models.Rate) float64 {
	sum := 0.0
	for _, v := range rates {
		sum += v.Rate
	}

	return float64(sum) / float64(len(rates))
}

func (h *Handler) StoreDailyExchangeData(c echo.Context) error {
	reqObj := &dtos.DailyExchangeDataRequest{}

	if err := h.bindAndValidate(c, reqObj); err != nil {
		return err
	}

	obj, err := h.Repo.FindExchangeByToAndFrom(reqObj.From, reqObj.To)

	if err != nil && err != gorm.ErrRecordNotFound {
		return c.JSON(http.StatusInternalServerError, "error db")
	}

	rate := &models.Rate{
		Date: reqObj.Date.Time,
		Rate: reqObj.Rate,
	}

	if obj == nil {
		obj = &models.Exchange{
			To:   reqObj.To,
			From: reqObj.From,
		}

		if err = h.Repo.CreateExchange(obj, nil); err != nil {
			return c.JSON(http.StatusInternalServerError, "error creating data")
		}
	}

	if err = h.Repo.AddRateToExchange(obj.ID, rate); err != nil {
		return c.JSON(http.StatusInternalServerError, "error adding exchange rate")
	}

	return c.NoContent(http.StatusCreated)
}
