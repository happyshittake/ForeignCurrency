package dtos

type WeekLastDataRequest struct {
	From string `json:"from" validate:"required"`
	To   string `json:"to" validate:"required"`
}

type WeekLastDataResponse struct {
	Avg      interface{} `json:"avg"`
	Variance interface{} `json:"variance"`
	Rates    []WeekItem  `json:"rates"`
}

type WeekItem struct {
	Date Datetime `json:"date"`
	Rate float64  `json:"rate"`
}
