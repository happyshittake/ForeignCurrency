package dtos

import (
	"encoding/json"
	"strings"
	"time"
)

const DateLayout = "2006-01-02"

type Datetime struct {
	time.Time
}

func (d *Datetime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	d.Time, err = time.Parse(DateLayout, s)
	return
}

func (d *Datetime) MarshalJSON() ([]byte, error) {
	str := d.Time.Format(DateLayout)
	return json.Marshal(str)
}

type DailyExchangeDataRequest struct {
	Date Datetime `json:"date" validate:"required"`
	From string   `json:"from" validate:"required"`
	To   string   `json:"to" validate:"required"`
	Rate float64  `json:"rate" validate:"required,gte=0"`
}
