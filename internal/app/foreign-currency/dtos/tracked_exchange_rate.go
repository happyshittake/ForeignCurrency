package dtos

type TrackedExchangeRateObject struct {
	From string      `json:"from"`
	To   string      `json:"to"`
	Rate interface{} `json:"rate"`
	Avg  interface{} `json:"avg"`
}
