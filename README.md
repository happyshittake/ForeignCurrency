# Foreign exchange

### test
- unit test on the handler
- integration test on the repository
- e2e comingsoon™️

### run the test
just set your environment:
 - TEST_DB_USERNAME=
 - TEST_DB_PASSWORD=
 - TEST_DB_HOST=
 - TEST_DB_PORT=
 - TEST_DB_SSL=
 - TEST_DB_NAME=
 
and then run go test ./...


### run the application
jus docker-compose up and then the application will be available on port 8080

### docs
- there's insomnia export file and HAR file in the api folder 
- for simple database schema there's db design picture in the root folder


that's all folks