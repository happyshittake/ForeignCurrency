FROM golang:latest as builder
WORKDIR /go/src/gitlab.com/happyshittake/ForeignCurrency
COPY ./cmd /go/src/gitlab.com/happyshittake/ForeignCurrency/cmd
COPY ./internal /go/src/gitlab.com/happyshittake/ForeignCurrency/internal
COPY ./vendor /go/src/gitlab.com/happyshittake/ForeignCurrency/vendor
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app gitlab.com/happyshittake/ForeignCurrency/cmd/foreign-currency

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/happyshittake/ForeignCurrency/app .
CMD ["./app"]
