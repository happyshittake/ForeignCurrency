package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/gommon/log"
	"gitlab.com/happyshittake/ForeignCurrency/internal/app/foreign-currency/bootstrap"
	"os"
)

func main() {
	userDB := os.Getenv("DB_USERNAME")
	passwordDB := os.Getenv("DB_PASSWORD")
	nameDB := os.Getenv("DB_NAME")
	hostDB := os.Getenv("DB_HOST")
	portDB := os.Getenv("DB_PORT")
	sslmodeDB := os.Getenv("DB_SSL")

	dbDSN := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		hostDB,
		portDB,
		userDB,
		nameDB,
		passwordDB,
		sslmodeDB,
	)

	db, err := gorm.Open("postgres", dbDSN)
	if err != nil {
		log.Fatal(err)
	}

	params := &bootstrap.Params{
		DB: db,
	}

	e := bootstrap.Bootstrap(params)

	e.Logger.Fatal(e.Start(":1323"))
}
